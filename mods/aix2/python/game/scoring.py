# -*- coding: iso-8859-15 -*-
# wookie sniper mod constants script by Scouty (30-03-2008)
# provides object scoring types for scoring and stats
# modified and included for the AIX mod custom scoring

# object scoring types

SCORING_TYPE_UNKNOWN            = 0
SCORING_TYPE_RIFLE              = 1
SCORING_TYPE_SNIPER             = 2

objects = {

# START - AIX V1.1 CONSTANTS

#CARBINE
		"g36v"					: SCORING_TYPE_RIFLE,
		"stg"					: SCORING_TYPE_RIFLE,
		"tavor"					: SCORING_TYPE_RIFLE,
		"steyr"					: SCORING_TYPE_RIFLE,
		"ebr"					: SCORING_TYPE_RIFLE,
                
#PISTOL
		"aix_gsh"				: SCORING_TYPE_RIFLE,
		"aix_gsh_silencer"			: SCORING_TYPE_RIFLE,
		"aix_glock19"				: SCORING_TYPE_RIFLE,
		"aix_glock19_silencer"			: SCORING_TYPE_RIFLE,
		"rupis_baghira"				: SCORING_TYPE_RIFLE,
		"rupis_baghira_silencer"		: SCORING_TYPE_RIFLE,
		"uspis_92fs"				: SCORING_TYPE_RIFLE,
		"uspis_92fs_silencer"			: SCORING_TYPE_RIFLE,
		"kit_beretta"				: SCORING_TYPE_RIFLE,
		"kit_gsh"				: SCORING_TYPE_RIFLE,
		"kit_uspmatch"				: SCORING_TYPE_RIFLE,
		"chpis_qsz92"				: SCORING_TYPE_RIFLE,
		"chpis_qsz92_silencer"			: SCORING_TYPE_RIFLE,
		"aix_uspmatch"				: SCORING_TYPE_RIFLE,
		"aix_uspmatch_silencer"			: SCORING_TYPE_RIFLE,
		"aix_beretta"				: SCORING_TYPE_RIFLE,
		"aix_beretta_silencer"			: SCORING_TYPE_RIFLE,

#SHOTGUN
		"aix_m41a_shot"				: SCORING_TYPE_RIFLE,
		"engineer_benelli_m4" 			: SCORING_TYPE_RIFLE,
		"engineer_jackhammer" 			: SCORING_TYPE_RIFLE,
		"engineer_norinco982"			: SCORING_TYPE_RIFLE,
		"engineer_protecta"			: SCORING_TYPE_RIFLE,
		"engineer_remington11-87"		: SCORING_TYPE_RIFLE,
		"engineer_saiga12"			: SCORING_TYPE_RIFLE,
#SMG
		"eurif_hk53a3"				: SCORING_TYPE_RIFLE,
		"hk53a3"				: SCORING_TYPE_RIFLE,
		"famas"					: SCORING_TYPE_RIFLE,
		"aix_mac11"				: SCORING_TYPE_RIFLE,
		"p220"					: SCORING_TYPE_RIFLE,
		"rurif_ak101"				: SCORING_TYPE_RIFLE,
		"rurif_ak101_b"				: SCORING_TYPE_RIFLE,
		"rurif_bizon"				: SCORING_TYPE_RIFLE,
		"rurrif_ak74u"				: SCORING_TYPE_RIFLE,
		"sasrif_mp7"				: SCORING_TYPE_RIFLE,
		"rurrif_ak74u_b"			: SCORING_TYPE_RIFLE,
		"usrif_mp5_a3"				: SCORING_TYPE_RIFLE,
		"kit_mac11"				: SCORING_TYPE_RIFLE,
		"at_bizon"				: SCORING_TYPE_RIFLE,
		"at_mp5"				: SCORING_TYPE_RIFLE,
		"medic_ak101"				: SCORING_TYPE_RIFLE,
		"specops_ak74u"				: SCORING_TYPE_RIFLE,
		"specops_hk53a3"			: SCORING_TYPE_RIFLE,
		"engineer_famas"			: SCORING_TYPE_RIFLE,
#ASSAULT
		"sa80gl"				: SCORING_TYPE_RIFLE,
		"sa80"					: SCORING_TYPE_RIFLE,
		"g36k"					: SCORING_TYPE_RIFLE,
		"xm8"					: SCORING_TYPE_RIFLE,
		"scar"					: SCORING_TYPE_RIFLE,
		"aix_ak5_tactical"			: SCORING_TYPE_RIFLE,
		"aix_famas"				: SCORING_TYPE_RIFLE,
		"eu_famas"				: SCORING_TYPE_RIFLE,
		"ak5"					: SCORING_TYPE_RIFLE,
		"sig552"				: SCORING_TYPE_RIFLE,
		"hk416"					: SCORING_TYPE_RIFLE,
		"f2000"					: SCORING_TYPE_RIFLE,
		"chrif_type95_b"			: SCORING_TYPE_RIFLE,
		"aix_fs2000"				: SCORING_TYPE_RIFLE,
		"gbrif_sa80a2_l85"			: SCORING_TYPE_RIFLE,
		"aix_mk14ebr"				: SCORING_TYPE_RIFLE,
		"aix_g36k_rif"				: SCORING_TYPE_RIFLE,
		"aix_type97"				: SCORING_TYPE_RIFLE,
		"aix_g36v"				: SCORING_TYPE_RIFLE,
		"aix_hk416"				: SCORING_TYPE_RIFLE,
		"aix_m41a"				: SCORING_TYPE_RIFLE,
		"aix_scarl_rif"				: SCORING_TYPE_RIFLE,
		"aix_tavor"				: SCORING_TYPE_RIFLE,
		"aix_sig552"				: SCORING_TYPE_RIFLE,
		"aix_sig552SpecOps"			: SCORING_TYPE_RIFLE,
		"aix_steyr_aug"				: SCORING_TYPE_RIFLE,
		"aix_xm8"				: SCORING_TYPE_RIFLE,
		"aix_p90"				: SCORING_TYPE_RIFLE,
		"aix_magpul"				: SCORING_TYPE_RIFLE,
		"chlmg_type95"				: SCORING_TYPE_RIFLE,
		"chrif_type85"				: SCORING_TYPE_RIFLE,
		"chrif_type95"				: SCORING_TYPE_RIFLE,
		"eurif_famas"				: SCORING_TYPE_RIFLE,
		"eurif_fnp90"				: SCORING_TYPE_RIFLE,
		"rurif_ak47"				: SCORING_TYPE_RIFLE,
		"rurif_ak47_b"				: SCORING_TYPE_RIFLE,
		"rurif_gp25"				: SCORING_TYPE_RIFLE,
		"rurif_gp30"				: SCORING_TYPE_RIFLE,
		"sasrif_fn2000"				: SCORING_TYPE_RIFLE,
		"sasrif_g36e"				: SCORING_TYPE_RIFLE,
		"usrif_m4"				: SCORING_TYPE_RIFLE,
		"usrif_fnscarl"				: SCORING_TYPE_RIFLE,
		"usrif_g36c"				: SCORING_TYPE_RIFLE,
		"usrif_g3a3"				: SCORING_TYPE_RIFLE,
		"usrif_m203"				: SCORING_TYPE_RIFLE,
		"assault_ak5"				: SCORING_TYPE_RIFLE,
		"assault_fn_fal"			: SCORING_TYPE_RIFLE,
		"assault_fn2000"			: SCORING_TYPE_RIFLE,
		"assault_fnscarl"			: SCORING_TYPE_RIFLE,
		"assault_g36k"				: SCORING_TYPE_RIFLE,
		"assault_g3a3"				: SCORING_TYPE_RIFLE,
		"assault_m41a"				: SCORING_TYPE_RIFLE,
		"assault_sa80a2"			: SCORING_TYPE_RIFLE,
		"engineer_hk416"			: SCORING_TYPE_RIFLE,
		"engineer_mk14ebr"			: SCORING_TYPE_RIFLE,
		"engineer_tavor"			: SCORING_TYPE_RIFLE,
		"medic_ak47"				: SCORING_TYPE_RIFLE,
		"medic_fs2000"				: SCORING_TYPE_RIFLE,
		"medic_g36e"				: SCORING_TYPE_RIFLE,
		"medic_m16a2"				: SCORING_TYPE_RIFLE,
		"medic_sa80"				: SCORING_TYPE_RIFLE,
		"medic_steyr_aug"			: SCORING_TYPE_RIFLE,
		"specops_aix_famas"			: SCORING_TYPE_RIFLE,
		"specops_fnscarl"			: SCORING_TYPE_RIFLE,
		"specops_g36c"				: SCORING_TYPE_RIFLE,
		"specops_m4"				: SCORING_TYPE_RIFLE,
		"specops_sg552"				: SCORING_TYPE_RIFLE,
		"specops_type95"			: SCORING_TYPE_RIFLE,
		"specops_xm8"				: SCORING_TYPE_RIFLE,
#SNIPER
		"l96"					: SCORING_TYPE_SNIPER,
		"tpg1"					: SCORING_TYPE_SNIPER,
		"dsr"					: SCORING_TYPE_SNIPER,
		"m109"					: SCORING_TYPE_SNIPER,
		"aix_as50"				: SCORING_TYPE_SNIPER,
		"aix_barrett_m109"			: SCORING_TYPE_SNIPER,
		"aix_dsr"				: SCORING_TYPE_SNIPER,
		"chsni_type88"				: SCORING_TYPE_SNIPER,
		"gbrif_l96a1"				: SCORING_TYPE_SNIPER,
		"rurif_dragunov"			: SCORING_TYPE_SNIPER,
		"usrif_m24"				: SCORING_TYPE_SNIPER,
		"sniper_as50"				: SCORING_TYPE_SNIPER,
		"sniper_dragunov"			: SCORING_TYPE_SNIPER,
		"sniper_dsr"				: SCORING_TYPE_SNIPER,
		"sniper_l96a1"				: SCORING_TYPE_SNIPER,
		"sniper_m109"				: SCORING_TYPE_SNIPER,
		"sniper_m24"				: SCORING_TYPE_SNIPER,
		"USSNI_M82A1"				: SCORING_TYPE_SNIPER,
		"ussni_m95_barret"			: SCORING_TYPE_SNIPER,
		"sniper_tpg1"				: SCORING_TYPE_SNIPER,
		"sniper_type88"				: SCORING_TYPE_SNIPER,
		"kit_as50"				: SCORING_TYPE_SNIPER,
		"aix_tpg1"				: SCORING_TYPE_SNIPER,
		"aix_vintorez"				: SCORING_TYPE_SNIPER,
#LMG	
		"hk21"					: SCORING_TYPE_RIFLE,
		"aix_type97_mg"				: SCORING_TYPE_RIFLE,
		"minimec"				: SCORING_TYPE_RIFLE,
		"mini"					: SCORING_TYPE_RIFLE,
		"aix_stg58"				: SCORING_TYPE_RIFLE,
		"eurif_hk21"				: SCORING_TYPE_RIFLE,
		"sasrif_mg36"				: SCORING_TYPE_RIFLE,
		"uslmg_m249saw"				: SCORING_TYPE_RIFLE,
		"rulmg_pkm"				: SCORING_TYPE_RIFLE,
		"rulmg_rpk74"				: SCORING_TYPE_RIFLE,
		"support_hk21"				: SCORING_TYPE_RIFLE,
		"support_m249saw"			: SCORING_TYPE_RIFLE,
		"support_mg36"				: SCORING_TYPE_RIFLE,
		"support_minigun"			: SCORING_TYPE_RIFLE,
		"support_minigun_mec"			: SCORING_TYPE_RIFLE,
		"support_pkm"				: SCORING_TYPE_RIFLE,
		"support_rpk74"				: SCORING_TYPE_RIFLE,
		"support_stg58"				: SCORING_TYPE_RIFLE,
		"support_type95"			: SCORING_TYPE_RIFLE,
		"aix_portableminigun"			: SCORING_TYPE_RIFLE,
		"aix_portableminigun_mec"		: SCORING_TYPE_RIFLE

# END - AIX V1.1 CONSTANTS

}

def getscoringtype(object):
    if object == None: return SCORING_TYPE_UNKNOWN

    template = object.templateName.lower()
    if objects.has_key(template):
        return objects[template]
    else:
        return SCORING_TYPE_UNKNOWN


