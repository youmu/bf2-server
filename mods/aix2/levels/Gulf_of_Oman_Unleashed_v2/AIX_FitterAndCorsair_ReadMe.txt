------------------------------------------------------------------------------------------------------------------------------------------------

Meshes and textures are copyright (c)2009 Clive Williams. 
Various included script/code technologies are owned by DICE and/or Electronic Arts.

------------------------------------------------------------------------------------------------------------------------------------------------

Included Aircraft:

Sukhoi Su-22, NATO Name 'Fitter'
Spawner name: AIX_Su22
Armed with a 4-bomb salvo, 4x rocket pods, and 30mm cannon. 
Texture Schemes: MEC grey (default), MEC Forest, MEC Desert, and MEC Night Ops, and United Nations (Polish) are all AIX compatible, and a Chinese plain metal texture has also been included

The Fitter began its life in the mid 50's as the Su-7, a very mediocre jet that could carry a good weapon load and a decent fuel capacity - its main problem was that it could not carry both at the same time. A later development invloved changing the outer wing sections to be variable-geometry, and also the fitting of a new engine that added afterburning, increased power, and greater fuel efficiency. The end result was an attack plane that sent a shockwave through NATO by exceeding their esitmated performance by an additional 150% - which is borne out by the fact that all these years later, Sukhoi is still offering a much-upgraded version of the Fitter to customers - the Su-22M4.

------------------------------------------------------------------------------------------------------------------------------------------------

Ling-Temco-Vought A-7 Corsair II a.k.a. 'SLUF' (Short Little Ugly F*cker)
Spawner name: AIX_A7
Armed with a 12-bomb salvo, 2x Zuni rocket pods, and 20mm Vulcan cannon. 
Texture Schemes: U.S. Navy low-visibility grey

The Corsair II is a development of the famous F-8 Crusader fighter, in a simplified form. Its long wing chord offers a lot of lifting power for only a moderate wingspan. Intended to replace the A-1, F-100, and F-105, it was one of the first aircraft to feature a HUD, and also one of the first to use a turbofan engine. (The A-7D was reported to use only one sixth of the fuel an F-100 Super Sabre would use at the same thrust.) The A-7 had the quickest and easiest development period of any U.S. combat aircraft since World War II. Pilots loved its flying characteristics and forward-view just as much as they hated its terrible crosswind-landing performance and lack of stopping/braking power on a wet carrier deck. Although the U.S. began retiring their A-7's way back in 1974, A-7's have also been exported to other countries and at least one - Greece - is still using them on active service, albeit in an advanced-upgrade form.

------------------------------------------------------------------------------------------------------------------------------------------------

Pros and Cons of the aircraft:
Obviously the Corsair has the edge in weaponry with its greater bomb load and beefier gun, but unlike the Fitter it has no afterburner. Although the Fitter's rockets are slightly weaker than the Corsair's Zunis, it carries double the amount and as such while under the control of a competent pilot has the potential to do more rocket damage than the Corsair could. The Corsair is also trickier to taxi and land due to its landing gear having a very narrow track width. When landing the Corsair, try to keep the fuselage as level as you can to avoid hitting the engine nozzle on the ground (which is often fatal)

------------------------------------------------------------------------------------------------------------------------------------------------

Before you begin:

* Smart modders always backup files before modifying them. 
* These aircraft are unsupported, are supplied as-is, and are used entirely at the user's own risk. 
* If you break your PC or lose data as a result of using these aircraft, your life is unfair.
* These aircraft may or may not work correctly in an online environment. If they do not, your life is unfair.
* Use of these aircraft assumes the user has a moderate to advanced knowledge of BF2 modification - If you do not have sufficient knowledge to install, use, and modify these aircraft, either research the info yourself or do not use the aircraft. I'm not your mother - do not email or PM me asking for help because you are a slackarse and can't be buggered looking for the answer yourself. (If however you want assistance with an issue of an advanced nature, I will help if possible)

If these aircraft are to be compiled into any distributed mod, please let me know for courtesy's sake.
If these aircraft are compiled into any distributed mod, they in no way becomes exclusive property of that mod.

------------------------------------------------------------------------------------------------------------------------------------------------

Basic Installation Instructions:

1. After unzipping the downloaded file, you should see one folder called AIX_Su22_A7 which contains three files, called:

Su22_A7_menu_client.zip
Su22_A7_objects_client.zip
Su22_A7_objects_server.zip

2. Drag this AIX_Su22_A7 folder into the main folder of the mod you want to use the planes in, i.e. \mods\aix2\

3. Copy and paste the line below at the top of the ServerArchives.con file (found in your mod's folder)

fileManager.mountArchive AIX_Su22_A7\Su22_A7_objects_server.zip Objects

4. Copy and paste the two lines below at the top of the ClientArchives.con file (found in your mod's folder)

fileManager.mountArchive AIX_Su22_A7\Su22_A7_objects_client.zip Objects
fileManager.mountArchive AIX_Su22_A7\Su22_A7_menu_client.zip Menu

5. To make a plane replace an existing plane in a map, edit the map's GameplayObjects.con for the game mode 
you want, and replace the existing aircraft spawner name with either AIX_A7 (for the Corsair II) or AIX_Su22 (for the Fitter)

example: 

ObjectTemplate.setObjectTemplate 2 AIX_Mirage_III

is changed to:

ObjectTemplate.setObjectTemplate 2 AIX_A7

...and re-zip the map's server.zip as per usual. When you next play the chosen map/gamemode, a Corsair will appear where the Mirage once was.More info on this procedure can be found at the BFSP Forums:
http://battlefieldsingleplayer.planetbattlefield.gamespy.com/forums/index.php

------------------------------------------------------------------------------------------------------------------------------------------------

Known issues:

These aircraft use HUD guiIndex numbers from the AIX or AIX2 mod. 
If used in other mods or in vanilla BF2 without proper adaption, the game may malfunction or crash.

------------------------------------------------------------------------------------------------------------------------------------------------

Terms:
If these planes are to be compiled into any distributed mod, please let me know for courtesy's sake.
If these planes are compiled into any distributed mod, they in no way become the exclusive property of that mod (though they have ownership of any improvements and updates they may implement.)
No matter how much extra shit you glue onto these plane, you didn't make them - I did. There is no magical point where they suddenly become your creation.
I reserve the right to update these planes as i see fit. I reserve the right to never update them again. I reserve the right to dip them in cheese and nail them to a tree if i so desire.
No fee is to be charged for this aircraft except for reasonable charges to cover downloading or media expenses where applicable. 
Wherever the planes go, this readme goes.

------------------------------------------------------------------------------------------------------------------------------------------------
ChangeLog:

Version 3 - adjusted armour effect threshold to fix missing crash sound
