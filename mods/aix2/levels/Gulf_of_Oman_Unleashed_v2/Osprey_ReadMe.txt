------------------------------------------------------------------------------------------------------------------------------------------------
Meshes and textures are copyright (c)2010 Clive Williams. 
Various included script/code technologies are owned by DICE and/or Electronic Arts.

Included Aircraft:

Boeing Vertol V-22 Osprey
Spawner name: AIX_V22 
Weapons: One retractable belly-mounted Gatling type gun. No other armament.
Crew: Pilot + Co-pilot/Gunner in the cockpit, and 6 paratroops in the rear section.

Before you begin:

* Smart modders always backup files before modifying them. 
* This aircraft is unsupported, is supplied as-is, and is used entirely at the user's own risk. 
* If you break your PC or lose data as a result of using this aircraft, hard luck.
* This aircraft may or may not work correctly in an online environment. If it does not, hard luck.
* Use of this aircraft assumes the user has an advanced knowledge of BF2 modification - If you do not have sufficient knowledge to install, use, and modify this aircraft, either research the info yourself or do not use the aircraft. I'm not your mother - do not email or PM me asking for help because you are a slacker and can't be stuffed looking for the answer yourself. (If however you want assistance with an issue of an advanced nature, I will help if possible)
*In order to include custom AI, it is necessary to hand modify the aiBehaviours.ai file. MAKE A BACKUP OF YOUR ORIGINAL FILES BEFORE YOU START. If done incorrectly, it has the potential to break your mod until it is fixed. It is not possible for me to supply a complete working file, as every mod's is different. If anyone ignores this warning and damages their aiBehaviours without making a backup first, i reserve the right to add their name to an 'Idiots List' and publicly insult them at my leisure.

If this aircraft is to be compiled into any distributed mod, please let me know for courtesy's sake.
If this aircraft is compiled into any distributed mod, it in no way becomes exclusive property of that mod: others still have permission to use the stand-alone version.

Quirks and known issues:

There will be some technical inaccuracies or physical flaws present. For example, the front half is similar to the early V-22 type with its larger footwell windows and narrower over head console. The rear half is more like an MV-22 with the smaller rear landing gear (for purposes of balance while on the ground) and the split rear ramp. If any of these inaccuracies bother you, go stand in front of a mirror. See that person there? Direct your complaints to them. Don't bother telling me, I couldn't care less.
This aircraft contains a spawn point, but bots will generally only use this in the air if you are a squad leader.
The object spawner should be set to 'TeamOnVehicle' to make the spawn point selectable while the Osprey is in flight.
The Co-pilot (Gunner) sees out through the sensor globe under the fuselage. He only has a view-screen for his view; he cannot look up. 
Bots will only take off forwards, they will never take off vertically like a helicopter. This is due to the hardcoding of the bot's core behaviours, and is not changeable by me. If you can't live with it, go watch TV instead. Of course humans are free to take off in whichever manner they choose.
The ramp and upper door close automatically, but only do so at high speed and medium height (about 250m) - this is to ensure that in most situations where the troopers are likely to parachute out, the doors will be open.
The 'Z4i0n the Bedwetter' picture is a separate removable object, which can be disabled in the V-22's tweak file if you don't want that little wanker soiling up your Osprey.***
The nacelles rotate directly in accordance with the throttle setting. There is good reasons for this. Purists may argue that in real life they take 20 seconds to traverse the full 90 degrees. The Osprey would have crossed the entire map in that time. Also, unless it was done this way, there would be no nacelle rotations with bot piloted Ospreys, due to the speed of their throttle movements.
For anyone wanting to re-paint this aircraft - the texture sheet is relatively small. Just deal with it. I had to.
Due to the nature of the belly-mounted gun, this Osprey is fairly devastating to anything from the horizon line downwards, but completely vulnerable to attack from above. That's not something that is easily rectified without adding extra weaponry in places there shouldn't be any.
Even though the occupant's icons (the blue dots on the vehicle icon) have been arrayed in the right spots, they may not display correctly when the Osprey has a full crew.
When you exit this aircraft you are placed just behind the ramp, facing backwards - just like you'd be if you were jumping from it in mid-air. It might be disorienting at first.

------------------------------------------------------------------------------------------------------------------------------------------------
Basic Installation Instructions:

1. After unzipping the downloaded file, you should see a passworded zip file* called AIX_Osprey. Unzip this file, so that you now have a folder called AIX_Osprey which contains these three files:

AIX_V22_client.zip
AIX_V22_server.zip
AIX_V22_menu.zip


2. Drag the AIX_Osprey folder into the main folder of the mod you want to use the Osprey in, i.e. \mods\aix2\

3. Copy and paste the line below at the TOP of the ServerArchives.con file (found in your mod's folder)

fileManager.mountArchive AIX_Osprey\AIX_V22_Server.zip Objects

4. Copy and paste the two lines below at the TOP of the ClientArchives.con file (found in your mod's folder)

fileManager.mountArchive AIX_Osprey\AIX_V22_Client.zip Objects
fileManager.mountArchive AIX_Osprey\AIX_V22_Menu.zip Menu

5. Now is the tricky bit. You will need to open your mod's ai/aiBehaviours.ai file, and copy these entries into the relevent sections. Take care with what you are doing! A typo or mistake here can be hard to trace.

In the section under the heading:
rem *** Set number of vehicles and id/name mappings ***
...where it has:
aiSettings.setVehicle Tank
aiSettings.setVehicle Plane
aiSettings.setVehicle Boat
aiSettings.setVehicle Infantery
etc. etc. etc, add this code:
aiSettings.setVehicle VTOLTransport
...to the end of that list.

6. In the section under the heading:
rem *** ParatrooperFixed Behaviour setup ***
...look for this line:
aiSettings.activateDefaultInterpreter ParatrooperFixed BAPSTLookAtWrapper Full
If it is present, go on to step 7. If it is NOT present, then add it in.

7. Now, copy and paste this code below between the From Here/To Here lines, down at the very end of the file right before the final entry (which is rem ***-------------------- ) 

>>> From Here (but not including this line) _____________________________________________

rem ***** VTOLTransport Behaviour setup ***
aisettings.setVehicleMods VTOLTransport StandardWeights
aiSettings.setVehicleBehaviour VTOLTransport Avoid BBAvoid BBPAvoidCollision3d 1 UCUnion PlaneWeights
aiSettings.setVehicleBehaviour VTOLTransport MoveTo BBMoveTo BBPGotoWaypoint3d 4 UCUnion HeliMoveWeights
aiSettings.setVehicleBehaviour VTOLTransport Idle BBIdle BBPIdle3d 5 UCUnion HeliMoveWeights
aiSettings.setVehicleBehaviour VTOLTransport Change BBChangeLandingCraft BBPChange 6 UCUnion ChangeInhibit
aiSettings.setVehicleDefaultBehaviour VTOLTransport Idle

aiSettings.addInterpreterEntry VTOLTransport InfoWrapper
aiSettings.addInterpreterEntry VTOLTransport HeliMoveTo
aiSettings.addInterpreterEntry VTOLTransport PlaneMoveToDirection
aiSettings.addInterpreterEntry VTOLTransport PlaneMoveToObject
aiSettings.addInterpreterEntry VTOLTransport HeliMoveAwayFromObject
aiSettings.addInterpreterEntry VTOLTransport Trigger
aiSettings.addInterpreterEntry VTOLTransport TriggerContinously
aiSettings.addInterpreterEntry VTOLTransport PlaneAimAt
aiSettings.addInterpreterEntry VTOLTransport HeliResetControls
aiSettings.addInterpreterEntry VTOLTransport Sense
aiSettings.activateDefaultInterpreter VTOLTransport BAPSTParallel Full
aiSettings.activateDefaultInterpreter VTOLTransport BAPSTSerial Full
aiSettings.activateDefaultInterpreter VTOLTransport BAPSTIf Full
aiSettings.activateDefaultInterpreter VTOLTransport BAPSTWait Full
aiSettings.activateDefaultInterpreter VTOLTransport BAPSTWhile Full
aiSettings.activateDefaultInterpreter VTOLTransport BAPSTWeaponWrapper Full
aiSettings.activateDefaultInterpreter VTOLTransport BAPSTFinishedWrapper Full
aiSettings.activateDefaultInterpreter VTOLTransport BAPSTUpdateVehicle Full
aiSettings.activateDefaultInterpreter VTOLTransport BAPSTChangeWeapon Full
aiSettings.activateDefaultInterpreter VTOLTransport BAPSTICTurnLock Full
aiSettings.activateDefaultInterpreter VTOLTransport BAPSTICChangeVehicle Full
aiSettings.activateDefaultInterpreter VTOLTransport BAPSTEvaluate Full
aiSettings.activateDefaultInterpreter VTOLTransport BAPSTNotifyBot Full
aiSettings.activateDefaultInterpreter VTOLTransport BAPSTIdle Full
aiSettings.activateDefaultInterpreter VTOLTransport BAPSTSetSensingAgentFocus Full
aiSettings.activateDefaultInterpreter VTOLTransport BAPSTSendRadioMessage Full
aiSettings.activateDefaultInterpreter VTOLTransport BAPSTDebug Full
aiSettings.activateDefaultInterpreter VTOLTransport BAPSTUseFlareOnMissileLock Full
aiSettings.activateDefaultInterpreter VTOLTransport BAPSTEvaluateCondition Full

>>> To Here (but not including this line) _____________________________________________

8. To make an Osprey replace an existing plane in a map, edit the map's GameplayObjects.con for the game mode 
you want, and replace the existing aircraft spawner name with AIX_V22

example: 

ObjectTemplate.setObjectTemplate 2 usair_f18

is changed to:

ObjectTemplate.setObjectTemplate 2 AIX_V22

...and re-zip the map's server.zip as per usual. more info on this procedure can be found at the BFSP Forums:
http://battlefieldsingleplayer.planetbattlefield.gamespy.com/forums/index.php

*To extract the files from the zip, enter "i_have_read_the_ReadMe" (case sensitive, and minus the quotes) when prompted. It was done this way in an attempt to get the minor bunch of idiots to actually read this ReadMe. My apologies to everyone else for making setup that much harder.
------------------------------------------------------------------------------------------------------------------------------------------------

***Who is 'Z4i0n the Bedwetter' and why have i picked on him?

Z4i0n is a half-arsed half-dick urine-soaked little scuzzbucket Brazilian hacker who sees himself as some kind of modern internet knight, fighting the good fight, which he achieves by disrupting various web sites and forums. One of his targets was battlefieldsingleplayer.com in 2009. I have no idea why he targeted BFSP; perhaps his sense of direction is as pitiful as his hacking skills, or maybe he thought we were a front for some political movement perhaps... Anyway, he brought the site down and it got repaired and brought back online again very quickly - at which point i made an observation about Z4i0n's personal hygiene, or lack thereof. Z4i0n then hacked the site again, telling me to 'get f*cked' which is quite ironic because that's exactly what i've been trying to get for the past couple of years, without any luck. Maybe i've lost my sex-appeal :(

Anyhow, if Z4i0n is after worldwide recognition, then recognition he shall get - starting with this Osprey. I included my tribute to Z4i0n as i perceive him to be, stuck onto the rear of the forward main bulkhead. Be careful of treading in piss-puddles on the floor around that area, as the guy is somewhat incontinent and leaves the 'golden trail' wherever he goes.

Z4i0n: "Doctor, I can't stop wetting myself."
Doctor: "I see, when did you notice this?"
Z4i0n: "When i took my bicycle clips off."
etc. etc. etc.

Z4i0n, admiring himself naked in front of the mirror: "Look Mum, 150 pounds of pure dynamite!"
Z4i0n's Mum: "Yeah, what a shame it only has a one-inch fuse."
etc. etc. etc.