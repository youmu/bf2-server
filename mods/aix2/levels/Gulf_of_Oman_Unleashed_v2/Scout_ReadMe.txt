------------------------------------------------------------------------------------------------------------------------------------------------
Meshes and textures are copyright (c)2010 Clive Williams. 
Various included script/code technologies are owned by DICE and/or Electronic Arts.

Included Aircraft:

Ron Wheeler/Skycraft Scout Mk. III Ultralight Aircraft
Spawner name: Scout_MkIII 
Weapons: None. Loud swearing and angry gestures permitted only.
Crew: Pilot only.

The Scout Mk. III

The Scout was one of the very first 'minimum aircraft' - i.e. it uses the bare minimum of equipment to allow a man to enter sustained, powered, controlled flight - and was one of the forerunners of the Microlite/Ultralight movement. It truly is 'ultra light' - the entire plane weighs less than 60kg without the pilot. These were originally developed for weekend flyers and for farmers to muster cattle or check their fences and so on. This is the Mk. III version, which had a bigger powerplant - because one thing the Scout was always a little short of was engine power. Because this plane flies low enough to see all the activity on the ground yet can cover a reasonable area, and has a visibility unmatched by any other aircraft, it makes an excellent observation platform and i have approximated this by giving it a wide-area radar. Instead of the usual tail-skid, my version of the plane has a tailwheel to allow you to steer while taxying.

Flying one of these things is a game of balancing speed versus height, and the enjoyment comes from mastering this and then making it work for you. This aircraft is old, underpowered, slow, small, and definitely non-aerobatic. The most fun comes from keeping your altitude quite low and zipping over or around the various obstacles you'll encounter. (In fact if you have my Flak guns on your map you'll need to keep low in order to survive) If you want to fly it at height in a straight line from A to B, sure you can do that, but it's probably going to be the most boring flight you've ever taken.

Before you begin:

* Smart modders always backup files before modifying them. 
* This aircraft is unsupported, is supplied as-is, and is used entirely at the user's own risk. 
* If you break your PC or lose data as a result of using this aircraft, stiff bikkies.
* This aircraft may or may not work correctly in an online environment. If it does not, stiff bikkies.
* Use of this aircraft assumes the user has an advanced knowledge of BF2 modification - If you do not have sufficient knowledge to install, use, and modify this aircraft, either research the info yourself or do not use the aircraft. I'm not your mother - do not email or PM me asking for help because you are a slacker and can't be stuffed looking for the answer yourself. (If however you want assistance with an issue of an advanced nature, I will help if possible)
*In order to include custom AI, it is necessary to hand modify the aiBehaviours.ai file. MAKE A BACKUP OF YOUR ORIGINAL FILES BEFORE YOU START. If done incorrectly, it has the potential to break your mod until it is fixed. It is not possible for me to supply a complete working file, as every mod's is different. Don't be a cheesedick. Make a backup first.

If this aircraft is to be compiled into any distributed mod, please let me know for courtesy's sake.
If this aircraft is compiled into any distributed mod, it in no way becomes exclusive property of that mod: others still have permission to use the stand-alone version.

Quirks and known issues:

This aircraft - naturally enough - flies very slow compared to regular BF2 aircraft. It's stall is gentle but fast, i.e. it will enter the stall smoothly but when it drops, it can drop very quickly. If your attitude is dead flat, i.e. no pitch or roll, then you can lose a great deal of height before the Wing objects start to work and tip you forwards so that you begin to gain control. What is more likely is that there won't be enough height for this to happen and the aircraft will hit the ground fast and explode. If you absolutely must fly and then throttle back, then take a little nose-down attitude before you do it, or lawn-dart shalt be thy name. 

Any aircraft with fixed undercarriage in a tail-dragger configuration runs the risk of standing on its nose if it hits a large bump or similar, and the Scout will do this too if you let it. If it does end up on its nose or it turns turtle, then quickly exit. It is rare to be able to recover the plane in this condition.

An exploding Scout doesn't do much damage so don't panic if you can't get far away before it goes bang.

Pilot exit speed is very low. You can jump out of the plane as soon as it touches the ground with little or no damage without needing to slow down first. The tailwheel brake on the plane will bring it to a halt quickly all by itself.

Aircraft of this type often have minor collisions and accidents, in fact the manufacturers *expect* this to occur. The Scout will sometimes let you bounce of things that would be fatal in a normal plane. What this means is occasionally a bot will abandon a Scout but it might not be destroyed when it hits the dirt. Often another bot will then come along and enter the plane, and there's a 99% chance said plane will be pointing at a tree or fence etc. and so he will sit there with his engine flat out, running forwards into the tree/fence until you intervene or he is killed.

The Scout has been set up so that (on MY system) at full throttle you can fly straight and level and then go into 'mouse cam' mode and look for enemies, and the plane will keep its level flight. Wide circles are also possible.

The tailwheel dropping causes this plane to bounce and skip forwards if spawned on a carrier. Any solutions to fix this issue unfortunately impacted the plane's behaviour on normal terrain, and so they were all rejected. I do not recommend you spawn this plane on a carrier deck. (You *might* have decent results if you spawn the plane at the correct angle so that the tailwheel is already down and therefore doesn't bounce. This angle is 12.5 degrees if you feel like trying)

------------------------------------------------------------------------------------------------------------------------------------------------
Basic Installation Instructions:

1. After unzipping the downloaded file, inside it's root folder you should see another folder called 'Scout_Ultralight' and also a couple of example files, and obviously there's this ReadMe :)


2. Drag the Scout_Ultralight folder into the main folder of the mod you want to use the Scout in, i.e. \mods\aix2\

3. Copy and paste the lines below at the TOP of the ServerArchives.con file (found in your mod's folder.)

fileManager.mountArchive Scout_Ultralight\ScoutObjects.zip Objects
fileManager.mountArchive Scout_Ultralight\ScoutMenu.zip Menu

(study the included example file if needed)

4. If you're using AIX2, you don't need to do this bit. If you're NOT using AIX2, you'll need to add the entries in 'Sample AIBehaviours.ai' into your mod's aiBehaviours.ai file in the appropriate places.

5. To make a Scout replace an existing plane in a map, edit the map's GameplayObjects.con for the game mode 
you want, and replace the existing aircraft spawner name with Scout_MkIII

example: 

ObjectTemplate.setObjectTemplate 2 usair_f18

is changed to:

ObjectTemplate.setObjectTemplate 2 Scout_MkIII

...and re-zip the map's server.zip as per usual. more info on this procedure can be found at the BFSP Forums:
http://www.battlefieldsingleplayer.com/forum/index.php

------------------------------------------------------------------------------------------------------------------------------------------------
"Have a stroke of its mane
It turns into a plane,
And then it turns back again, 
when you tug on it's..."
