Map updated for Coop 1.3
Map by Homegrown
Mercenaries Mod by Hayabusa

Navmeshed by HomeGrown using Kysteramas Tutorial. Navmesh fixed by Kysterama.

Map only tested in these Mods:
Mercenaries and BF2

Map has custom snow skins. Not the greastest job, more or less just a test.

Notable bugs that I cant find answers too: When in commander mode you have to zoom in on commander map to place Artillery or UAV. When zoomed out the 2 wont go where you click.   
Any problems or you just want to make a comment. Please feel free to yell at me at devils6302@comcast.net or you can find me at http://www.dies-world.com/ just remember this is my first map and I am still learning. Map is on the large size.

Special Thanks to:
Hayabusa for putting up with all my questions.
Kysterama for his SP tutorial and fixing my navmesh.
http://www.dies-world.com/
http://battlefieldsingleplayer.planetbattlefield.gamespy.com/
http://kysterama.co.nr/
http://bfeditor.org/forum/

Map Testers:
Woodburner
Hayabusa
Andremio
Blankared
Snake
LtVendetta
Bravo
chasingpink
Tide
and anyone that I missed.

Loading music by Judas Priest. This was for me only and wrote this just in case I forget to remove song.
Album: Angel Of Retribution 
Song:  Revolution


****************Mission****************
We have reason to believe Insurgents are using the Karkand Mountains to transport WMD from there main city. Your mission Lt. Hayabusa and Sgt.Tide is to take your squad of Mercenaries and make your way through the Karkand Mountains and capture all there bases. Once you have control of all Mountain bases you will then have to take over the city. This will be no easy task as there will be heavy resistance. Good luck Mercs 