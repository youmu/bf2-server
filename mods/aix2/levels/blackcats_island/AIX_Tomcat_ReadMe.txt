------------------------------------------------------------------------------------------------------------------------------------------------
Tomcat for BF2 - For free use by the BF2 community. You are free to modify or repaint this aircraft as you like and include it in any mod you like. (Please observe the terms outlined further down)

Made by clivewil, May 2009
Meshes and textures are copyright (c)2009 Clive Williams. 
Various included script/code technologies are owned by DICE and/or Electronic Arts.

Included Aircraft: Grumman F-14 Tomcat. (generic)

Spawner name: AIX_F14

Included Texture Schemes:
US Navy Jolly Rogers Low-vis (default texture)
AIX MEC Desert
AIX MEC Forest
AIX MEC Woodland (unique to this jet)
AIX MEC default (named AIX_F14_c_nightOps.dds - rename to AIX_F14_c.dds to use as default texture for MEC)

Armed with: 2x 500lb bomb, 2x Sidewinder AAMs (RIO control) 2x Phoenix BVR AAMs, 16x Zuni 5" rockets and 1x Vulcan cannon.

The Zuni pods and triple-ejector racks are separate items to the main mesh and can be removed independently by inserting "rem " into their lines in the tweak. Note that if the TERs are removed but the Zunis are not, the Zunis will float in space and your plane will look stupid. These items are geometry only and have no effect on the Zuni launcher weapon itself.

Before you begin:

* Smart modders always backup files before modifying them. 
* This aircraft is unsupported, is supplied as-is, and is used entirely at the user's own risk. 
* If you break your PC or lose data as a result of using this aircraft, that's just really sad.
* This aircraft may or may not work correctly in an online environment. If it does not, that's just really sad too.
* Use of this aircraft assumes the user has a moderate to advanced knowledge of BF2 modification - If you do not have sufficient knowledge to install, use, and modify this aircraft, either research the info yourself or do not use the aircraft. I'm not your mother - do not email or PM me asking for help because you are a sloth and can't be buggered looking for the answer yourself. (If however you want assistance with an issue of an advanced nature, I will help if possible)

Terms:
If the Tomcat is to be compiled into any distributed mod, please let me know for courtesy's sake.
If the Tomcat is compiled into any distributed mod, it in no way becomes the exclusive property of that mod (though they have ownership of any improvements and updates they may implement.)
No matter how much extra shit you glue onto this plane, you didn't make it - I did. There is no magical point where it suddenly becomes your creation.
I reserve the right to update this plane as i see fit. I reserve the right to never update it again. I reserve the right to paint it blue, tip it over and set it on fire if I so wish.
No fee is to be charged for this aircraft except for reasonable charges to cover downloading or media expenses where applicable. 
Wherever the plane goes, this readme goes.
------------------------------------------------------------------------------------------------------------------------------------------------
Basic Installation Instructions:

1. After unzipping the downloaded file, you should see one folder called AIX_Tomcat which contains three files, called:

Tomcat_MenuClient.zip
Tomcat_client.zip
Tomcat_server.zip

2. Drag this AIX_Tomcat folder into the main folder of the mod you want to use the F-14 in, i.e. \mods\aix2\

3. Copy and paste the line below at the top of the ServerArchives.con file (found in your mod's folder)

fileManager.mountArchive AIX_Tomcat\Tomcat_server.zip Objects

4. Copy and paste the two lines below at the top of the ClientArchives.con file (found in your mod's folder)

fileManager.mountArchive AIX_Tomcat\Tomcat_client.zip Objects
fileManager.mountArchive AIX_Tomcat\Tomcat_MenuClient.zip Menu

5. To make a Tomcat replace an existing plane in a map, edit the map's GameplayObjects.con for the game mode you want, and replace the existing aircraft spawner name with AIX_F14

Example: 

ObjectTemplate.setObjectTemplate 2 AIX_Mirage_III

is changed to:

ObjectTemplate.setObjectTemplate 2 AIX_F14

...and re-zip the map's server.zip as per usual. more info on this procedure can be found at the BFSP Forums:
http://battlefieldsingleplayer.planetbattlefield.gamespy.com/forums/index.php

Known issues:

This aircraft uses HUD guiIndex numbers from the AIX or AIX2 mod. 

If used in other mods or in vanilla BF2 without proper adaption, the game may malfunction or crash. 

The aircraft contains a spawn point, but bots will generally only use these if you are a squad leader.

The landing gear is complex and takes slightly longer to extend than on most other jets. Make a low slow flat approach, and use the gear indicator to know when it's safe to touch down. 

Exiting is disallowed from the rear seat position, to stop bot RIOs from bailing out while inverted. Seat-swapping still works, however. To exit the aircraft, you will need to be in the pilot position.

The RIO only has a pair of sidewinders for armament. I don't really give a rat's festering left nut about the RIO, and so I gave him whichever weapon I would miss least. You're lucky i didn't give him just a newspaper and cup of tea instead. So long as his position exists with a working weapon to use as an example, I've done my job.

The AIM-54 is a beyond-visual-range AAM and has a behaviour similar to the AIX JASSM - if it loses its lock, or is fired without one, it will seek its own target. In absence of an enemy target, it will seek a friendly. The AIM-54 is a one-shot-one-kill missile. Use caution and make sure of your target prior to launching. (It is essentially the same as the Phantom's AIM-7 but has double the range - 2000m)

Bots are reluctant to use the full 2000m range of the AIM-54s.

There's a certain amount of mixing between variants in this model, e.g. small chin radome but large upgraded engines. Stop being a hair-splitter and just accept it. 

------------------------------------------------------------------------------------------------------------------------------------------------