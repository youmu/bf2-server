*** Allied Intent Xtended 2.0 Server Release Notes ***
*** Nov 2008 ***

For support and discussion, please visit the official website and forums at http://www.aixtended.com
Official forums located at -> http://forums.aixtended.com


TABLE OF CONTENTS

I.	License Information
II.	Contact Information
III.	Installation Instructions
IV.	Notes


I.  	License Information

	A.  Copyrights and Warranty
	* BF2 game content and materials copyright Electronic Arts Inc. and its licensors.
	* All non-EA and non-DICE media, including but not limited to logos, web content and references,
	  image files, models, skins, audio, video, and other media files are copyright Allied Intent Xtended and Allied Intent 2005-
	  2007 and its development team.
	* AIX MOD IS PROVIDED "AS IS" WITHOUT ANY WARRANTIES OF ANY KIND, EXPRESS OR IMPLIED.
	  ANY USE YOU CHOOSE TO MAKE OF THESE TOOLS & MATERIALS IS UNDERTAKEN BY YOU ENTIRELY AT YOUR OWN
	  RISK. NEITHER EA NOR AIX WARRANT THAT THESE TOOLS & MATERIALS WILL NOT CAUSE DAMAGE TO
	  YOUR COMPUTER SYSTEM, NETWORK, SOFTWARE OR OTHER TECHNOLOGY.
	
	B.  Usage and Distribution
	* AIX Server may be used and distributed by any entity, public or private, WITHOUT modification to the files contained within the zip-archive.
	* No entity may extract AIX content for any other use or re-distribute AIX content without express written permission from the Dev team!
	* No entity may extract any content from AIX and distribute it seperately including all in-game content, maps, music, sounds, textures, models and effects.
	* No entity may make monetary charges for the distribution or usage of the AIX Server, except   MINOR COSTS for distribution medium.
	* AIX Server may be included in game sampler distributions. Per EA requirements, it must be stated that a installation of Battlefield 2 Server is required for usage of the mod.


II.  	Contact Information

	A.  For support and discussion, please visit the official website and forums at http://www.AIXtended.com
	* technical support requests must be directed to the official website forums -> http://forums.aixtended.com
	* Frequently asked questions about AIX located at -> http://aixtended.com/faqV3.html
	
	B.  For business and official correspondence, please contact the AIX mod devs at devteam@aixtended.com or reach kysterama at kysterama@hotmail.com


III. 	Installation Instructions

	1.) These serverfiles work on windows and on linux servers.
	2.) Copy/Upload mods/aix2 including all subfolders and files to your <bf2server-root> .
	2.) Modify <bf2server-root>/mods/aix2/settings/serversettings.con to fit your bf2server-environment (IP, Port, etc.)
	3.) Modify <bf2server-root>/mods/aix2/settings/maplist.con to reflect the mapcycle you want to run.
	4.) Modify <bf2server-root>/mods/aix2/ai/aidefault.ai to reflect the botsettings you want to run.


IV. 	Notes

	* The setting sv.novehicle 1 just works in gpm_cq.
	* To host a Infantryonly (sv.novehicle 1) game with bots, select as gamemode gpm_cq and add "+ai 1" to the commandline launching your bf2server.
		e.g. <bf2server>\bf2_w32ded.exe +modPath mods/aix2 -autostart -noquitprompts +ai 1
	* "+ai 1" can be added aswell to config.xml of bf2ccd.exe
		e.g. <GameExecArgs>+ai 1</GameExecArgs>
		
		

*** AIXTENDED.com and AIXSTATISTICS.com is sponsored and hosted by http://www.i3D.net ***